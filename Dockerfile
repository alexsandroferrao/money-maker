FROM openjdk:11-jdk-slim
EXPOSE 8080
ADD money-maker-backend/target/money-maker-backend-1.0-SNAPSHOT-runner.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]