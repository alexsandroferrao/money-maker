package basetest;

import client.MoneyMakerClient;
import org.testng.annotations.BeforeSuite;
import static io.restassured.RestAssured.enableLoggingOfRequestAndResponseIfValidationFails;

public class BaseTest {

    public static MoneyMakerClient moneyMakerClient;

    @BeforeSuite
    public void setUp(){
        enableLoggingOfRequestAndResponseIfValidationFails();
        moneyMakerClient = new MoneyMakerClient();
    }
}
