package client;

import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import model.Money;
import org.apache.http.HttpStatus;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.is;

public class MoneyMakerClient {

    public ValidatableResponse getAllMoneys(){
        return when().
                    get("/moneys").
               then().
                    contentType(ContentType.JSON).
                    statusCode(HttpStatus.SC_OK);
    }

    public ValidatableResponse getMoneyByName(String name){
        return given().
                    pathParam("name", name).
                when().
                    get("/moneys/{name}").
               then().
                    contentType(ContentType.JSON).
                    statusCode(HttpStatus.SC_OK);
    }

    public ValidatableResponse getHealthCheck(){
        return when()
                    .get("/health/live")
                .then().
                    statusCode(HttpStatus.SC_OK).
                    contentType(ContentType.JSON).
                    body("status",is("UP"));
    }

    public ValidatableResponse postMoneys(Money money){
        return given().
                    contentType(ContentType.JSON).
                    body(money).
               when().
                    post("/moneys").
               then().
                    statusCode(HttpStatus.SC_CREATED);
    }
    public ValidatableResponse deleteMoney(Money money){
        return given().
                    contentType(ContentType.JSON).
                    body(money).
               when().
                    delete("/moneys").
               then().
                    statusCode(HttpStatus.SC_ACCEPTED);
    }

    public ValidatableResponse putMoney(Money money){
        return given().
                    contentType(ContentType.JSON).
                    body(money).
               when().
                    put("/moneys").
               then().
                    statusCode(HttpStatus.SC_OK);
    }
}