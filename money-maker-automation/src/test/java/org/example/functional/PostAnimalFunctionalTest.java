package org.example.functional;

import basetest.BaseTest;
import model.Animal;
import model.Money;
import org.testng.annotations.Test;

public class PostAnimalFunctionalTest extends BaseTest {

    @Test
    public void validaPostAnimal(){

        Animal animal = new Animal("Leoa");
        Money money = new Money(animal, 1000);

        moneyMakerClient.postMoneys(money);

    }
}
