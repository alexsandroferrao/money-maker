package org.example.functional;

import basetest.BaseTest;
import model.Animal;
import model.Money;
import org.testng.annotations.Test;

public class PutAnimalFunctionalTest extends BaseTest {

    @Test
    public void validaAtualizacaoAnimalTest(){

        Animal animal = new Animal("Animal1");
        Money money = new Money(animal, 10000);

        moneyMakerClient.postMoneys(money);

        money.getAnimal().setNome("Animal2");

        moneyMakerClient.putMoney(money);

    }
}
