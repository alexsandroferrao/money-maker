package org.example.contract;

import basetest.BaseTest;
import io.restassured.module.jsv.JsonSchemaValidator;
import org.testng.annotations.Test;
import java.io.File;
import static org.apache.http.HttpStatus.SC_OK;

public class ContractTest extends BaseTest {

    @Test
    public void contractGet(){

        moneyMakerClient.getAllMoneys().
                statusCode(SC_OK)
                .body(JsonSchemaValidator.matchesJsonSchema(new File("src/test/resources/schema/money_schema_get.json").getAbsoluteFile()));

    }
}
