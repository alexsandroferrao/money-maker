package org.example.aceitacao;

import basetest.BaseTest;
import model.Animal;
import model.Money;
import org.testng.annotations.Test;
import static org.hamcrest.CoreMatchers.*;

public class MoneyAcceptanceTest extends BaseTest {

    @Test
    public void acceptanteMoneyTest(){

        Animal animal = new Animal("Garfield");
        Money money = new Money(animal, 10000);

        moneyMakerClient.postMoneys(money);

        moneyMakerClient.getAllMoneys().body("animal.nome",hasItem(animal.getNome()));

        money.getAnimal().setNome("Tom");

        moneyMakerClient.putMoney(money);

        moneyMakerClient.getMoneyByName(money.getAnimal().getNome()).body("animal.nome",is(money.getAnimal().getNome()));

        moneyMakerClient.deleteMoney(money);

    }
}
