package org.example.service;

import org.example.model.Animal;
import org.example.model.Money;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.Response;
import java.util.*;
import java.util.stream.Collectors;

@ApplicationScoped
public class MoneyService {

    private final Set<Money> moneys = new HashSet<>();

    public MoneyService() {
        moneys.addAll(Arrays.asList(
                new Money(new Animal("Tartaruga"), 2),
                new Money(new Animal("Garça"), 5),
                new Money(new Animal("Arara"), 10),
                new Money(new Animal("Mico-leão-dourado"), 20),
                new Money(new Animal("Onça"), 50),
                new Money(new Animal("Garoupa"), 100),
                new Money(new Animal("Lobo-guará"), 200)
        ));
    }

    public Response add(Money money) {
        if(getByName(money.getAnimal().getNome()) != null){
            return Response.noContent().status(204).build();
        }else{
            moneys.add(money);
            return Response.noContent().status(201).build();
        }
    }

    public List<Money> showAllList() {
        return moneys
                .stream().sorted(
                        Comparator.comparing(Money::getReais)).collect(Collectors.toList());
    }

    public Money getByName(String name) {
        return moneys.stream()
                .filter(mon -> name.equalsIgnoreCase(mon.getAnimal().getNome()))
                .findAny()
                .orElse(null);
    }

    public void update(Money moneyBody) {
        remove(moneyBody);
        add(moneyBody);
    }

    public void remove(Money moneyBody) {
        moneys.removeIf(money -> money.getReais().equals(moneyBody.getReais()));
    }
}
