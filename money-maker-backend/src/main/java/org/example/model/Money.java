package org.example.model;

public class Money {

    private Animal animal;
    private Integer reais;

    private Money() {
    }

    public Money(Animal animal, Integer value) {
        this.animal = animal;
        this.reais = value;
    }

    public Animal getAnimal() {
        return animal;
    }

    public Integer getReais() {
        return reais;
    }

}
