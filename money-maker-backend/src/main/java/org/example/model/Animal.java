package org.example.model;

public class Animal {
    private String nome;

    private Animal() {
    }

    public Animal(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }
}
