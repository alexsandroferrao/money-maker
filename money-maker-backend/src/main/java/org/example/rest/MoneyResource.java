package org.example.rest;

import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.example.model.Money;
import org.example.service.MoneyService;


import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/moneys")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class MoneyResource {

    @Inject
    MoneyService moneyService;

    public MoneyResource() {
        //contrutor do recurso
    }

    @GET
    public List<Money> getMoneys() {
        return moneyService.showAllList();
    }

    @GET
    @Path("/{animal}")
    @Produces(APPLICATION_JSON)
    @Consumes(APPLICATION_JSON)
    @APIResponse(responseCode = "200")
    @APIResponse(responseCode = "404", description = "Não foi encontrado nenhum animal")
    public Response moneyName(@PathParam("animal") String animal) {
        Money money = moneyService.getByName(animal);

        if (money != null) {
            return Response.ok().status(200).entity(money).build();
        }
        return Response.ok().status(404).entity("NOT FOUND").build();
    }

    @POST
    @APIResponse(responseCode = "201")
    @APIResponse(responseCode = "204")
    @Produces(APPLICATION_JSON)
    @Consumes(APPLICATION_JSON)
    public Response createMoney(Money moneyBody) {
        return moneyService.add(moneyBody);
    }

    @DELETE
    @APIResponse(responseCode = "202")
    public Response deleteMoney(Money moneyBody) {
        moneyService.remove(moneyBody);
        return Response.accepted().status(202).build();
    }

    @PUT
    @APIResponse(responseCode = "200")
    public Response putMoney(Money moneyBody) {
        moneyService.update(moneyBody);
        return Response.accepted().status(200).build();
    }
}
