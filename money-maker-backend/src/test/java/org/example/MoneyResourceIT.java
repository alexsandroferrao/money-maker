package org.example;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import org.example.model.Animal;
import org.example.model.Money;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
class MoneyResourceIT {

    private static final String MONEYS = "moneys";

    private MoneyResourceIT(){}

    @BeforeAll
    public static void setupRestAssured(){
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    }

    @Test
    void showAllListDefault() {
        when().get(MONEYS).
                then().
                statusCode(200).
                body("$.size()", is(7));
    }

    @Test
    void shouldCreatedMoney() {
        Money moneyCachorro = new Money(new Animal("cachorro"), 99);
        given().
                contentType(JSON).
                body(moneyCachorro).
                when().
                post(MONEYS).
                then().
                statusCode(201);
    }

    @Test
    void shouldDeleteMoney() {
        Money moneyCachorro = new Money(new Animal("Arara"), 10);
        given().
                contentType(JSON).
                body(moneyCachorro).
                when().
                delete(MONEYS).
                then().
                statusCode(202);
    }

    @Test
    void shouldUpdateMoney() {
        Money moneyCachorro = new Money(new Animal("Leão"), 10);
        given().
                contentType(JSON).
                body(moneyCachorro).
                when().
                put(MONEYS).
                then().
                statusCode(200);
    }
}
