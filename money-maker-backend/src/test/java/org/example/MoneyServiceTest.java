package org.example;

import org.example.model.Animal;
import org.example.model.Money;
import org.example.service.MoneyService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertEquals;

class MoneyServiceTest {
    private List<Money> moneyList = new ArrayList<>();

    private MoneyServiceTest(){}

    @BeforeEach
    public void setupListaDeMoneys() {
        moneyList.addAll(Arrays.asList(
                new Money(new Animal("Tartaruga"), 2),
                new Money(new Animal("Garça"), 5),
                new Money(new Animal("Arara"), 10),
                new Money(new Animal("Mico-leão-dourado"), 20),
                new Money(new Animal("Onça"), 50),
                new Money(new Animal("Garoupa"), 100),
                new Money(new Animal("Lobo-guará"), 200)
        ));
    }

    @Test
    void deveMostrarTodaLista() {
        MoneyService moneyService = new MoneyService();
        List<Money> moneys = moneyService.showAllList();

        assertEquals(moneys.size(), moneyList.size());
    }

    @Test
    void deveAdicionarNovoItemNalista() {
        MoneyService moneyService = new MoneyService();
        Money novoMoney = new Money(new Animal("Teste"), 30);
        Response response = moneyService.add(novoMoney);

        Money moneyAnimal = moneyService.getByName("Teste");

        assertEquals(201, response.getStatus());
        assertEquals("Teste", moneyAnimal.getAnimal().getNome());
    }

    @Test
    void naoDeveAdicionarItemRepetidoNalista() {
        MoneyService moneyService = new MoneyService();
        Money novoMoney = new Money(new Animal("Tartaruga"), 2);
        Response response = moneyService.add(novoMoney);

        assertEquals(204, response.getStatus());
    }

    @Test
    void deveRemoverDaLista() {
        MoneyService moneyService = new MoneyService();
        Money novoMoney = new Money(new Animal("Tartaruga"), 2);
        moneyService.remove(novoMoney);

        assertThat(moneyService.showAllList(), not(hasItem(novoMoney)));
    }

    @Test
    void deveAtualizarItemDaLista() {
        MoneyService moneyService = new MoneyService();
        Money novoMoney = new Money(new Animal("Abelha"), 2);
        moneyService.update(novoMoney);

        assertThat(moneyService.showAllList(), hasItem(novoMoney));
    }
}
